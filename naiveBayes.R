library(data.table)
library(NLP)
library(tm)
library(wordcloud)
library(e1071)
library(SnowballC)
library(randomForest)
library(rpart)
train_raw <- read.csv("train.csv", stringsAsFactors = FALSE)
test_raw <- read.csv("test.csv", stringsAsFactors = FALSE)
test_raw_labels <- read.csv("test_labels.csv", stringsAsFactors = FALSE)
fullTest<-merge(test_raw,test_raw_labels, by = "id")
fullTest$toxic <- ifelse (fullTest$toxic==0,0,1)
fullTest$severe_toxic <- ifelse (fullTest$severe_toxic==0,0,1)
fullTest$obscene <- ifelse (fullTest$obscene==0,0,1)
fullTest$threat <- ifelse (fullTest$threat==0,0,1)
fullTest$insult <- ifelse (fullTest$insult==0,0,1)
fullTest$identity_hate <- ifelse (fullTest$identity_hate==0,0,1)
fullTest$YN <- ifelse (rowMeans(fullTest[,3:8])>0, 'Toxic','Not toxic')
fullTest$YN <- as.factor(fullTest$YN)


train_raw$YN <- ifelse (rowMeans(train_raw[,3:8])>0, 'Toxic','Not toxic')
train_raw$YN <- as.factor(train_raw$YN)

#marge the train and test to make the corpus#
corp<-rbind(train_raw, fullTest)

corp$toxic <- as.factor(corp$toxic)
corp$severe_toxic <- as.factor(corp$severe_toxic)
corp$obscene <- as.factor(corp$obscene)
corp$threat <- as.factor(corp$threat)
corp$insult <- as.factor(corp$insult)
corp$identity_hate <- as.factor(corp$identity_hate)

corpus <- Corpus(VectorSource (corp$comment_text))

#cleansing
corpus_clean <- tm_map(corpus,content_transformer(tolower))
corpus_clean <- tm_map(corpus_clean,removeNumbers)
corpus_clean <- tm_map(corpus_clean,removeWords, stopwords())
corpus_clean <- tm_map(corpus_clean,removePunctuation)
corpus_clean <- tm_map(corpus_clean,stemDocument)
corpus_clean <- tm_map(corpus_clean,stripWhitespace)


#make a dtm
dtm <- DocumentTermMatrix(corpus_clean)
dim(dtm)

#clean by frequent words (15 times or more)
frequent_dtm<- DocumentTermMatrix(corpus_clean, list(dictionary = findFreqTerms(dtm,15)))
dim(frequent_dtm)

####################################################################################################

pal <- brewer.pal(8,'Dark2')
wordcloud (corpus_clean, min.freq = 15, random.order = FALSE, color = pal)
wordcloud (corpus_clean[corp$YN=='Toxic'], min.freq = 15, random.order = FALSE, color = pal)
wordcloud (corpus_clean[corp$YN=='Not toxic'], min.freq = 15, random.order = FALSE, color = pal)

####################################################################################################



#split back to test and train by the order
train_raw1 <- corp[1:159571,]
test_raw1 <-corp[159572:312735,]

#split the frequent_dtm to train and test by the order
train_dtm <- frequent_dtm[1:159571,]
test_dtm <- frequent_dtm[159572:312735,]

#conversion function
conv_yesno <- function(x){
  x <- ifelse (x>0,1,0)
  x <- factor(x,levels = c(1,0), labels = c('Yes','No'))
}

#shurter run time than as.data.frame(as.matrix)
test_dtm <- removeSparseTerms(test_dtm, 0.95)
train_dtm <- removeSparseTerms(train_dtm, 0.95)

#split the apply to 3 part because it was too havey to run at once
train1 <- apply(train_dtm[1:50000,],MARGIN = 1:2,conv_yesno)
train2 <- apply(train_dtm[50001:100000,],MARGIN = 1:2,conv_yesno)
train3 <- apply(train_dtm[100001:159571,],MARGIN = 1:2,conv_yesno)


#build back to one and clean memory
train12<-rbind(train1, train2)
dim(train12)
train1<- NULL
train2<- NULL
train<- rbind(train12,train3)
train3 <-NULL
train12 <- NULL
dim(train)

#split the apply to 6 part because it was too havey to run at once
test1 <- apply(test_dtm[1:25000,],MARGIN = 1:2,conv_yesno)
test2 <- apply(test_dtm[25001:50000,],MARGIN = 1:2,conv_yesno)

test12 <- rbind(test1,test2)
dim(test12)
test1 <-NULL
test2 <- NULL

test3 <- apply(test_dtm[50001:75000,],MARGIN = 1:2,conv_yesno)
test4 <- apply(test_dtm[75001:100000,],MARGIN = 1:2,conv_yesno)
test34 <- rbind(test3,test4)
dim(test34)
test3 <-NULL
test4 <- NULL

test1234 <- rbind(test12,test34)
dim(test1234)
test12 <-NULL
test34 <- NULL

test5 <- apply(test_dtm[100001:125000,],MARGIN = 1:2,conv_yesno)
test6 <- apply(test_dtm[125001:153164,],MARGIN = 1:2,conv_yesno)

test56<- rbind(test5,test6)
dim(test56)
test5 <-NULL
test6<- NULL

test<- rbind(test1234,test56)
dim(test)
test1234<NULL
test56<-NULL


df_train <- as.data.frame (train)
df_test <- as.data.frame (test)



dim(df_train)
dim(train)
dim(df_test)
dim(test)
#add the toxic/not toxic column to the df's
df_train$YN <- train_raw1$YN
df_test$YN <- test_raw1$YN

dim(df_train)
dim(df_test)
dim(train_raw1)
dim(test_raw1)

View(df_train)
View(df_test[1,40:44])
View(df_train1[1,40:43])
df_train1<- df_train[,-3]
df_train1<- df_train1[,-(10:11)]
df_train1<- df_train1[,-12]
df_train1<- df_train1[,-24]
df_train1<- df_train1[,-(26:28)]
df_train1<- df_train1[,-(35:37)]
df_train1<- df_train1[,-(36:38)]
df_train1<- df_train1[,-37]
df_train1<- df_train1[,-39]
df_train1<- df_train1[,-(44:45)]
df_test1 <- df_test[,-41]
df_train1$YN <- train_raw1$YN
df_train1 <- df_train1 [,-43]
dim(df_train1)
dim(df_test1)


df_train1 <- cbind(df_train1, train_raw1[,3:8])
df_test1 <- cbind(df_test1, test_raw1[,3:8])



#################################naiveBayes########################################
toxic <- naiveBayes(df_train1[,-43],df_train1$toxic)
predToxic1<- predict(toxic, df_test1 [,-43])

severe_toxic <- naiveBayes(df_train1[,-43],df_train1$severe_toxic)
predSevere_toxic1<- predict(severe_toxic, df_test1 [,-43])


obscene <- naiveBayes(df_train1[,-43],df_train1$obscene)
predObscene1<- predict(obscene, df_test1 [,-43])

threat <- naiveBayes(df_train1[,-43],df_train1$threat)
predThreat1<- predict(threat, df_test1 [,-43])

insult <- naiveBayes(df_train1[,-43],df_train1$insult)
predInsult1<- predict(insult, df_test1 [,-43])

identity_hate <- naiveBayes(df_train1[,-43],df_train1$identity_hate)
predIdentity_hate1<- predict(identity_hate, df_test1 [,-43])

YN <- naiveBayes(df_train1[,-43],df_train1$YN)
predYN <- predict(YN,df_test1[,-43])

table(predToxic1,df_test1$toxic) #precision:0.9998  recall:0.9994

table(predSevere_toxic1,df_test1$severe_toxic) #precision:0.9951  recall:0.976

table(predObscene1,df_test1$obscene) #precision:0.9989  recall:0.9996

table(predThreat1,df_test1$threat) #precision:0.995  recall:0.976

table(predInsult1,df_test1$insult) #precision:0.9993  recall:0.9994

table(predIdentity_hate1,df_test1$identity_hate) #precision:0.9975  recall:0.991

table(predYN,df_test1$YN) #precision:1  recall:0.999


TN <-57735
FN <- 90
FP <- 0
TP <- 95339

PRECISION <- TP/(TP+FP)
RECALL <- TP/(TP+FN)

PRECISION
RECALL

predToxic<- predict(toxic, df_test1 [,-43],"raw")
predSevere_toxic<- predict(severe_toxic, df_test1 [,-43],"raw")
predObscene<- predict(obscene, df_test1 [,-43],"raw")
predThreat<- predict(threat, df_test1 [,-43],"raw")
predInsult<- predict(insult, df_test1 [,-43],"raw")
predIdentity_hate<- predict(identity_hate, df_test1 [,-43],"raw")

allmatrix<- rbind(predToxic[,2],predSevere_toxic[,2],predObscene[,2],predThreat[,2],predInsult[,2],predIdentity_hate[,2])
label <- c('toxic','severe_toxic','obscene','threat','insult','identity_hate')
rownames(allmatrix) <- label

predictor <- rownames(allmatrix)[apply(allmatrix, 2, which.max)]

table(predictor,test_raw1$YN)
table(predictor,test_raw1$)
